const path = require('path')
function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  // eslint-loader 是否在保存的时候检查
  lintOnSave: true,
  // 基本路径
  publicPath: './',
  outputDir: "dist", // 项目打包输出目录
  assetsDir: "static", // 项目静态文件打包输出目录
  // 配置true会有.map文件，打包后体积大, false打包后不会有.map文件，打包后体积小 生产环境配置false
  productionSourceMap: process.env.NODE_ENV !== 'production',
  // 配置全局路径
  chainWebpack: (config) => {
    config.entry.app = ['babel-polyfill', './src/main.js']
    config.resolve.alias.set('@images', resolve('./src/assets/images'))
    config.resolve.alias.set('@', resolve('./src'))
    config.module.rule('file').test(/\.mkv$/)
      .include.add(path.resolve(__dirname, './src/assets/video'))
      .end()
      .use('url-loader')
      .loader('file-loader')
  },
  // 配置本地服务
  devServer: {
    port: 9999,
    open: process.env.NODE_ENV !== 'production',
    https: false,
    hotOnly: false,
    // 配置代理，解决跨域问题
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        // 生产环境
        target: 'https://yzdz.yuanzhizhiqi.com/robotweb',
        // 仝康本地
        // target: 'http://10.2.34.201:8090/',
        changeOrigin: true,
        ws: false,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    }
  }
}
