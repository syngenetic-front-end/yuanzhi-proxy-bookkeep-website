import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Base',
    redirect: '/home',
    component: () => import('@/layout'),
    children: [
      {
        path: 'home',
        name: 'Home',
        component: () => import('@/views/Home')
      },
      {
        path: 'news',
        name: 'News',
        component: () => import('@/views/News/List/index')
      },
      {
        path: 'news/detail/:id',
        name: 'NewsDetail',
        props:true,
        component: () => import('@/views/News/Detail/index')
      },
      {
        path: 'about-us',
        name: 'AboutUs',
        component: () => import('@/views/AboutUs')
      },
    ]
  },
  {
    path: '/404',
    name: 'NotFund',
    component: () => import('@/views/404'),
  },
  { path: '*', redirect: '/404', }
]

const originalPush = VueRouter.prototype.push

VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const router = new VueRouter({
  // mode: 'hash',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 }
  }
})
router.beforeEach(async (to, from, next) => {
  // 设置页面title
  // document.title = to.meta.title
  next();
})
export default router
