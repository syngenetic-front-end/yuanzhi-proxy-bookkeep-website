import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    activeNavName: 'Home',
    windowScrollTop: 0,
    isShowLayer: false
  },
  mutations: {
    setActiveNavName(state, val) {
      state.activeNavName = val
    },
    setWindowScrollTop(state, val) {
      state.windowScrollTop = val
    },
    setIsShowLayer(state, val) {
      state.isShowLayer = val
    },
  }
})