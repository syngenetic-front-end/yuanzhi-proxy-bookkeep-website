import request from '@/utils/request'
const curryRequest = (url, method, trModuleCode, trCode, data) => {
  return request(`index/news/${url}`, method, trModuleCode, trCode, data)
}
// 首页轮播图列表查询
// cateId	类别ID(1、财税资讯 2、财税百科)
export function apiQueryIndexNewsList(data) {
  return curryRequest('queryIndexNewsList', 'get',data)
}
// 查询单个新闻详情
export function apiQueryIndexNewsById(data) {
  return curryRequest('queryIndexNewsById', 'get',data)
}