import request from '@/utils/request'
const curryRequest = (url, method, trModuleCode, trCode, data) => {
  return request(`index/${url}`, method, trModuleCode, trCode, data)
}
// 首页轮播图列表查询
export function apiQueryIndexRotationList() {
  return curryRequest('rotation/queryIndexRotationList', 'get')
}
// 发送验证码
export function apiSendCode(data) {
  return curryRequest('appointment/sendCode', 'get',data)
}
// 预约体验
export function apiExperience(data) {
  return curryRequest('appointment/register', 'post',data)
}


