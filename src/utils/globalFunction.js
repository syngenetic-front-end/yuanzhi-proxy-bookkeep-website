/**
 * Created by wzy on 2018/12/07.
 *
 */
export default {
  // 节流
  throttle(fn, context, args, delay = 10) {
    let previous = 0
    return function (...args) {
      // 获取当前时间，转换成时间戳，单位毫秒
      let now = Date.now()
      // 将当前时间和上一次执行函数的时间进行对比
      // 大于等待时间就把 previous 设置为当前时间并执行函数 fn
      if (now - previous > delay) {
        previous = now
        fn.apply(context, args)
      }
    }
  },
  /**
  * 获取随机字符串
  * @param length 要获取的字符串的长度
  * @return 指定长度的随机字符串
  */
  randomStr(length) {
    let str = Math.random().toString(36).substr(2);
    if (str.length > length) return str.substr(0, length);
    str += this.randomStr(length - str.length);
    return str
  },
  /**
  * 数值按照指定位数补零
  * @params num:原数值
  * @params index:补零后位数，如不传，则默认为2位 必须number类型
 */
  getZero(num, index = 2) {
    if ((!num && num !== 0) || isNaN(num)) return num;
    if (typeof num === 'string' && num !== 0) {
      num = Number(num);
    }
    if (num < 0) {
      return '暂不支持负数'
    }
    for (var i = 1; i < index; i++) {
      if (num === 0) {
        num += '0'
      } else if (num > 0) {
        if (Math.floor(num) < Math.pow(10, i)) {
          num = "0" + num;
        }
      }
    }
    return num;
  },
  /**
  * 时间戳转换成日期
  * @params date:时间戳
  * @params days:指定延后天数 默认0
  * @params fmt:格式，如不传，则默认为YYYY-MM-DD HH:mm:ss格式
  */
  getFormatDate(date, days = 0, fmt = 'YYYY-MM-DD HH:mm:ss') {
    if (!date) return false;
    // 取得浏览器的userAgent字符串
    const userAgent = navigator.userAgent;
    const isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; // 判断是否IE<11浏览器
    const isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
    if (isIE || isIE11) {
      date = date.replace(/-/g, '/')
    }
    let _date, time;
    if (typeof date === 'object') {
      _date = date.valueOf();
      time = new Date(_date + 86400000 * days);
    } else if (typeof date === 'number') {
      time = new Date(date + 86400000 * days)
    } else if (typeof date === 'string') {
      _date = new Date(date).valueOf();
      time = new Date(_date + 86400000 * days);
    }
    const year = time.getFullYear();
    const month = this.getZero(time.getMonth() + 1);
    const day = this.getZero(time.getDate());
    const hour = this.getZero(time.getHours());
    const minte = this.getZero(time.getMinutes());
    const second = this.getZero(time.getSeconds());
    switch (fmt) {
      case 'YYYY-MM-DD':
        return `${year}-${month}-${day}`
      case 'YYYY-MM-DD HH:mm:ss':
        return `${year}-${month}-${day} ${hour}:${minte}:${second}`
      case 'YYYY-MM-DD HH:mm':
        return `${year}-${month}-${day} ${hour}:${minte}`
      case 'YYYY-MM':
        return `${year}-${month}`
      case 'YYYY:MM:DD HH:mm:ss':
        return `${year}:${month}:${day} ${hour}:${minte}:${second}`
      case 'YYYYMMDDhhmmss':
        return `${year}${month}${day}${hour}${minte}${second}`
      case 'YYYY:MM:DD HH:mm':
        return `${year}:${month}:${day} ${hour}:${minte}`
      case 'YYYY:MM:DD':
        return `${year}:${month}:${day}`
      case 'YYYY:MM':
        return `${year}:${month}`
      case 'YYYY.MM':
        return `${year}.${month}`
      case 'YYYY.MM.DD':
        return `${year}.${month}.${day}`
      case 'YYYY.MM.DD HH:mm':
        return `${year}.${month}.${day} ${hour}:${minte}`
      case 'YYYYMMDD':
        return `${year}${month}${day}`
    }
  },
}
