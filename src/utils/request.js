import axios from 'axios'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 10000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent

    return config
  },
  error => {
    // do something with request error
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data
    if (res.header.code !== 0) {
      alert(res.header&&res.header.msg||'Error')
      return Promise.reject(res || 'Error')
    } else {
      return res
    }
  },
  error => {
    return Promise.reject(error)
  }
)
/**
 * 封装接口请求方法
 * @param url 域名后需补齐的接口地址
 * @param method 接口请求方式
 * @param data data下的其他数据体
 */
const request = (url, method, data) => {
    // 处理get请求参数拼接
    if ((method === "get" || method === "GET") && data) {
      url += "?";
      for (const key in data) {
        if (!url.endsWith("?")) url += "&&";
        url += `${key}=${data[key]}`;
      }
    }
  return service({
    url,
    method,
    data
  })
}

export default request
