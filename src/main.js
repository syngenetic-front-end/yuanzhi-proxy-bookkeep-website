import Vue from 'vue'
import App from './App.vue'
import router from './router'
import '@/styles/index.scss' // 全局css
import globalFunction from '@/utils/globalFunction.js' // 全局css
import VueAwesomeSwiper from "vue-awesome-swiper";
import 'swiper/dist/css/swiper.min.css'
import store from './store'

Vue.prototype.globalFunction = globalFunction
Vue.use(VueAwesomeSwiper)
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
